function calculator(operator, num1, num2) {
    let result;

    [num1, num2].forEach(element => {
        if(typeof element !== "number"){
            throw "Invalid arg";
        }
    });

    switch(operator){
    case("+"):
        result = num1 + num2;
        break;
    case("-"):
        result = num1 - num2;
        break;
    case("*"):
        result = num1 * num2;
        break;
    case("/"):
        result = num1 / num2;
        break;
    default:
        result = "Invalid operator";
        break;
    }
    return result;
    //console.log(result);
}

// calculator("+", 2, 3);
// calculator("-", 2, 3);
// calculator("*", 2, 3);
// calculator("/", 2, 3);
// calculator("a", 2, 3);

export {calculator};